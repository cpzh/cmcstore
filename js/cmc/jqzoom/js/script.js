jQuery('document').ready(function(){

    var options = {
        zoomType: 'innerzoom',
        zoomWidth: 478,
        zoomHeight: 478,
        preloadImages: false,
        xOffset: 0,
        yOffset: 0,
        imageOpacity: 0.6,
        title : false
    };
    jQuery('#main-image').jqzoom(options);
    jQuery('.ro-tabs > label > a').click(function(){
        //jQuery('.ro-tabs label').removeClass('active');
        //jQuery(this).parent('label').addClass('active');
        jQuery('.ro-tabs label').css('border-color','#e0e0e0');
        jQuery(this).parent('label').css('border-color','#00bddd');
    })

})